const mongoose = require("mongoose");

const User = require("../Models/UserModel");

const companySchema = new mongoose.Schema({
  field: {
    type: String,
    required: true,
    trim: true,
  },
  location: {
    type: String,
    required: true,
    trim: true,
  },
  webSite: {
    type: String,
    required: true,
    trim: true,
  },
  description: {
    type: String,
    required: true,
    trim: true,
  },
  jobs: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Job"
    }],
    
    admin: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Admin"
    }]
},
);
module.exports = User.discriminator("Company", companySchema);
