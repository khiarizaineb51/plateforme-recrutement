const mongoose = require ('mongoose')

const jobSchema = new mongoose.Schema ({

titleJob :{
    type:String,
    required: true,
    trim: true
},
category :{
    type:String,
    required: true,
    trim: true
},
typeContrat :{
    type:String,
    required: true,
    trim: true
},
dateJob :{
    type:String,
    required: true,
    trim: true
},
dateFinJob :{
    type:String,
    required: true,
    trim: true
},
skills : {
    type:String,
    required: true,
    trim: true
},
companys:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Company"
},
categorys:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Category"
},
candidates:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"Candidate"
}

},{timestamps:true}
)
module.exports=mongoose.model("Job", jobSchema)