const mongoose = require("mongoose");

const User = require("../Models/UserModel");

const CandidateSchema = new mongoose.Schema(
  {
    address: {
      type: String,
      required: true,
    },

    phone: {
      type: Number,
      required: true,
    },

    resumes: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Resume",
      },
    ],

    jobs: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Job",
    },
  },
 
);

module.exports = User.discriminator("candidate", CandidateSchema);
