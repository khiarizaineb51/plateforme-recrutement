const mongoose= require ('mongoose')

const candidacySchema = new mongoose.Schema({

score : {
    type: Number,
    required : true,
    trim : true
},
dateCand :{
    type: Number,
    required:true,
    trim: true
}

}, {timestamps: true}
)
module.exports=mongoose.model("candidacy", candidacySchema)