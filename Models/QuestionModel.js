const mongoose =require('mongoose')

const QuestionSchema = new mongoose.Schema ({

iDQuestion : {
    type:Number,
    required: true,
    trim: true
},
candidate:[{
    type:mongoose.Schema.Types.ObjectId,
    ref:"Candidate"
}]
},{timestamps:true}
)
module.exports= mongoose.model("question", QuestionSchema)