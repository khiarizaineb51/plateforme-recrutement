const mongoose =require('mongoose')

const RecommendationSchema = new mongoose.Schema ({
descRecom:{
    type: String,
    required : true,
    trim: true
},
candidates:{
    type: mongoose.Schema.Types.ObjectId,
    ref:"Candidate"
}
},{timestamps:true}
)
module.exports= mongoose.model("recommendation", RecommendationSchema)