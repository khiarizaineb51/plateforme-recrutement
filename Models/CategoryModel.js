const mongoose = require ('mongoose')

const CategorySchema= new mongoose. Schema({

name:{
    type:String,
    required: true,
    trim: true
},
description: {
    type: String,
    required: true,
  },
jobs :[{
    type: mongoose.Schema.Types.ObjectId,
      ref: "Job"
}]

}, {timestamps:true}
)
module.exports=mongoose.model("category",CategorySchema)