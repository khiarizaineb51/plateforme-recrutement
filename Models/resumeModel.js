const mongoose=require ('mongoose')

const resumeSchema = new mongoose.Schema ({
    idCV :{
        type: Number,
        required : true,
        trim: true
    },
    dateR : {
        type: Number,
        required : true,
        trim: true
    },
    description : {
        type: String,
        required : true,
        trim: true
    },
    candidate: [
        {
            Type:mongoose.Schema.Types.ObjectId,
            ref: "Candidate"
        }
    ]
})
module.exports= mongoose.model('Resume',resumeSchema)