const userModel = require("../Models/UserModel");
const bcrypt = require("bcrypt");
const Joi = require("joi");
const jwt = require("jsonwebtoken"); 

module.exports = {
  //Signup
  register: async (req, res) => {
    // data validation
    const schema = Joi.object({
      name: Joi.string().min(3).max(50).required(),
      email: Joi.string().required().email(),
      password: Joi.string().min(8).max(200).required(),
    });

    const { error } = schema.validate(req.body); 
    if (error) {
      return res.status(422).json(error);
    }

    // existance par mail
    const user = await userModel.findOne({ email: req.body.email });

    if (user) {
      return res.status(400).json("user with this email is already exist");
    }

    //hachage password
    const hash = await bcrypt.hash(req.body.password, 10);

    req.body.password = hash;

    console.log(req.body);

    //create user
    userModel.create(req.body, (err, user) => {
      if (err) {
        res.status(422).json(err);
      } else {
        res.status(201).json(user);
      }
    });
  },

  //login
  login: async (req, res) => {
    
    // data validation
    const schema =Joi.object({
      email: Joi.string().required().email(),
      password: Joi.string().min(8).max(200).required()
     })
    const { error } = schema.validate(req.body);
    if (error) {
      return res.status(422).json(error);
    }

    //recherche
    const user = await userModel.findOne({email});

    if (!user) {
      return res.status(400).json({ message: "does not email" });
    }

    // compare password
    const isverify = await bcrypt.compare(password, user.password);
    if (isverify) {
      const token = await jwt.sign({ id: user._id }, "jobgate");
      res.status(200).json({
        message: " user found",
        token: token,
      });
    } else {
      res.status(400).json({ message: " invalid password" });
    }
  },

  logout: (req, res, next) => {
    var refreshToken = req.body.refreshToken;

    if (this.refreshToken in tokensList) {
      delete tokensList[this.refreshToken];
    }
    res
      .status(200)
      .json({ status: 200, message: "logout account", data: null });
  },


  uploadavatar: (req, res) => {
    const data = {
      avatar: req.file.filename,
    };

    userModel.findByIdAndUpdate({ _id: req.user.id }, data, (err, user) => {
      if (err) {
        res.status(500).json({ message: "avatar not uploaded" });
      } else {
        userModel.findById({ _id: user._id }, (error, item) => {
          if (error) {
            res.json("error");
          } else {
            res.status(200).json({
              message: "user updated",
              data: item,
            });
          }
        });
      }
    });
  },
  updateUserPassword : async (req, res) => {
    const { oldPassword, newPassword } = req.body;
    if (!oldPassword || !newPassword) {
      throw new CustomError.BadRequestError('Please provide both values');
    }
    const user = await userModel.findOne({ _id: req.user.userId });
  
    const isPasswordCorrect = await user.comparePassword(oldPassword);
    if (!isPasswordCorrect) {
      throw new CustomError.UnauthenticatedError('Invalid Credentials');
    }
    user.password = newPassword;
  
    await user.save();
    res.status(StatusCodes.OK).json({ msg: 'Success! Password Updated.' });
  }
  
};
