const candidacyModel =require ('../Models/CandidacyModel')

module.exports={

    create: (req, res) => {
        const Candidacy = new candidacyModel(req.body);
        Candidacy.save(function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: " error" + err, data: null });
          } 
          else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },

      readAll: (req, res) => {
        candidacyModel.find({}, function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: "error" + err, data: null });
          } 
          else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
      readOne: (req, res) => {
        candidacyModel.findById({ _id: req.params.id }, function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: "error" + err, data: null });
          }
           else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
      update: (req, res) => {
        candidacyModel.findByIdAndUpdate(
          { _id: req.params.body.id },
          req.body,
          (err, item) => {
            if (err) {
    
              res.status(406).json({ status: 406, message: "error" + err, data: null });
            }
             else {
    
              res.status(200).json({ status: 200, message: "success", data: item });
            }
          }
        );
      },

      delete: (req, res) => {
        candidacyModel.findByIdAndDelete({ _id: req.params.id }, (err, item) => {
          if (err) {
            res
              .status(406)
              .json({ status: 406, message: "error" + err, data: null });
          } else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
}