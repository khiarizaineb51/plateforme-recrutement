const Resumemodel=require('../Models/resumeModel')

module.exports={
    create: (req, res) => {
        const Resume = new Resumemodel(req.body);
        Resume.save(function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: " error" + err, data: null });
          } 
          else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
      readAll: (req, res) => {
        Resumemodel.find({}, function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: "error" + err, data: null });
          } 
          else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },

      readOne: (req, res) => {
        Resumemodel.findById({ _id: req.params.id }, function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: "error" + err, data: null });
          }
           else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
      update: (req, res) => {
        Resumemodel.findByIdAndUpdate(
          { _id: req.params.body.id },
          req.body,
          (err, item) => {
            if (err) {
    
              res.status(406).json({ status: 406, message: "error" + err, data: null });
            }
             else {
    
              res.status(200).json({ status: 200, message: "success", data: item });
            }
          }
        );
      },
      delete: (req, res) => {
        Resumemodel.findByIdAndDelete({ _id: req.params.id }, (err, item) => {
          if (err) {
            res
              .status(406)
              .json({ status: 406, message: "error" + err, data: null });
          } else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
}
