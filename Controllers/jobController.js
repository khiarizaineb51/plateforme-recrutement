const jobModel = require ('../Models/jobModel')

module.exports ={
    create: (req, res) => {
        const Job = new jobModel(req.body);
        Job.save(function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: " error" + err, data: null });
          } 
          else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
      readAll: (req, res) => {
        jobModel.find({}, function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: "error" + err, data: null });
          } 
          else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },  
      readOne: (req, res) => {
        jobModel.findById({ _id: req.params.id }, function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: "error" + err, data: null });
          }
           else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
     
      update: (req, res) => {
        jobModel.findByIdAndUpdate(
          { _id: req.params.body.id },
          req.body,
          (err, item) => {
            if (err) {
    
              res.status(406).json({ status: 406, message: "error" + err, data: null });
            }
             else {
    
              res.status(200).json({ status: 200, message: "success", data: item });
            }
          }
        );
      },

      delete: (req, res) => {
        jobModel.findByIdAndDelete({ _id: req.params.id }, (err, item) => {
          if (err) {
            res
              .status(406)
              .json({ status: 406, message: "error" + err, data: null });
          } else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
}
