const companyModel = require('../Models/CompanyModel')

module.exports = {

create: (req, res) => {
    const company = new companyModel(req.body);
    company.save(function (err, item) {
      if (err) {
        res.status(406).json({ status: 406, message: " error" + err, data: null });
      } 
      else {
        res.status(200).json({ status: 200, message: "success", data: item });
      }
    });
  },

  readAll: (req, res) => {
    companyModel.find({}, function (err, item) {
      if (err) {
        res.status(406).json({ status: 406, message: "error" + err, data: null });
      } 
      else {
        res.status(200).json({ status: 200, message: "success", data: item });
      }
    });
  },
  readOne: (req, res) => {
    companyModel.findById({ _id: req.params.id }, function (err, item) {
      if (err) {
        res.status(406).json({ status: 406, message: "error" + err, data: null });
      }
       else {
        res.status(200).json({ status: 200, message: "success", data: item });
      }
    });
  },
  update: (req, res) => {
    companyModel.findByIdAndUpdate(
      { _id: req.params.body.id },
      req.body,
      (err, item) => {
        if (err) {

          res.status(406).json({ status: 406, message: "error" + err, data: null });
        }
         else {

          res.status(200).json({ status: 200, message: "success", data: item });
        }
      }
    );
  },

  delete: (req, res) => {
    companyModel.findByIdAndDelete({ _id: req.params.id }, (err, item) => {
      if (err) {
        res
          .status(406)
          .json({ status: 406, message: "error" + err, data: null });
      } else {
        res.status(200).json({ status: 200, message: "success", data: item });
      }
    });
  },
}