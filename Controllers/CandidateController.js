const candidateModel = require ('../Models/CandidateModel');

module.exports = {
  create: (req, res) => {
    const Candidate = new candidateModel(req.body);
    Candidate.save(function (err, item) {
      if (err) {
        res.status(406).json({ status: 406, message: " error" + err, data: null });
      } 
      else {
        res.status(200).json({ status: 200, message: "success", data: item });
      }
    });
  },
  readAll: (req, res) => {
    candidateModel.find({}, function (err, item) {
      if (err) {
        res.status(406).json({ status: 406, message: "error" + err, data: null });
      } 
      else {
        res.status(200).json({ status: 200, message: "success", data: item });
      }
    });
  },
  readOne: (req, res) => {
    candidateModel.findById({ _id: req.params.id }, function (err, item) {
      if (err) {
        res.status(406).json({ status: 406, message: "error" + err, data: null });
      }
       else {
        res.status(200).json({ status: 200, message: "success", data: item });
      }
    });
  },
  update: (req, res) => {
    candidateModel.findByIdAndUpdate(
      { _id: req.params.body.id },
      req.body,
      (err, item) => {
        if (err) {

          res.status(406).json({ status: 406, message: "error" + err, data: null });
        }
         else {

          res.status(200).json({ status: 200, message: "success", data: item });
        }
      }
    );
  },
 delete: (req, res) => {
    candidateModel.findByIdAndDelete({ _id: req.params.id }, (err, item) => {
      if (err) {
        res
          .status(406)
          .json({ status: 406, message: "error" + err, data: null });
      } else {
        res.status(200).json({ status: 200, message: "success", data: item });
      }
    });
  },
};
