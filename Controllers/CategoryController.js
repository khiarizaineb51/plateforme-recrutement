const categoryModel= require ('../Models/CategoryModel')

module.exports= {

    create: (req, res) => {
        const category = new categoryModel(req.body);
        category.save(function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: " error" + err, data: null });
          } 
          else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
      readAll: (req, res) => {
        categoryModel.find({}, function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: "error" + err, data: null });
          } 
          else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },

      readAll: (req, res) => {
        candidateModel.find({}, function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: "error" + err, data: null });
          } 
          else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
      readOne: (req, res) => {
        categoryModel.findById({ _id: req.params.id }, function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: "error" + err, data: null });
          }
           else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
      update: (req, res) => {
        categoryModel.findByIdAndUpdate(
          { _id: req.params.body.id },
          req.body,
          (err, item) => {
            if (err) {
    
              res.status(406).json({ status: 406, message: "error" + err, data: null });
            }
             else {
    
              res.status(200).json({ status: 200, message: "success", data: item });
            }
          }
        );
      },
      delete: (req, res) => {
        categoryModel.findByIdAndDelete({ _id: req.params.id }, (err, item) => {
          if (err) {
            res
              .status(406)
              .json({ status: 406, message: "error" + err, data: null });
          } else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
    };


