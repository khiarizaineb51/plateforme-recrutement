const recommendationModel= require ('../Models/RecommendationModel')
 
module.exports= {
    create: (req, res) => {
        const Recommendation = new recommendationModel(req.body);
        Recommendation.save(function (err, item) {
          if (err) {
            res
              .status(406)
              .json({ status: 406, message: " error" + err, data: null });
          } else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },

      readAll: (req, res) => {
        recommendationModel.find({}, function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: "error" + err, data: null });
          } 
          else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },

      readOne: (req, res) => {
        recommendationModel.findById({ _id: req.params.id }, function (err, item) {
          if (err) {
            res.status(406).json({ status: 406, message: "error" + err, data: null });
          }
           else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },

      update: (req, res) => {
        recommendationModel.findByIdAndUpdate(
          { _id: req.params.body.id },
          req.body,
          (err, item) => {
            if (err) {
    
              res.status(406).json({ status: 406, message: "error" + err, data: null });
            }
             else {
    
              res.status(200).json({ status: 200, message: "success", data: item });
            }
          }
        );
      },
      delete: (req, res) => {
        recommendationModel.findByIdAndDelete({ _id: req.params.id }, (err, item) => {
          if (err) {
            res
              .status(406)
              .json({ status: 406, message: "error" + err, data: null });
          } else {
            res.status(200).json({ status: 200, message: "success", data: item });
          }
        });
      },
    };


