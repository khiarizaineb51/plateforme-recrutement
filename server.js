const express = require("express");
const db = require("./Config/DataBase");
const bodyparser = require("body-parser");
require("dotenv").config();
const logger = require("morgan");

const app = express();

app.use(logger("dev"));

//for handling json data
app.use("/", (req, res) => {
  res.json({ message: "salut" });
});
//parse application/ x-www-form-urlencoded
app.use(express.urlencoded({ extended: false }));

const userRouter = require("./Routers/UserRouter");
const adminRouter = require("./Routers/AdminRouter");
const candidateRouter = require("./Routers/CandidateRouter");
const companyRouter = require("./Routers/CompanyRouter");
const jobRouter = require("./Routers/jobRouter");
const candidacyRouter = require("./Routers/CandidacyRouter");
const resumeRouter = require("./Routers/resumeRouter");
const recommendationRouter = require("./Routers/RecommendationRouter");
const testRouter = require("./Routers/TestRouter");
const categoryRouter = require("./Routers/CategoryRouter");
const questionRouter = require("./Routers/QuestionRouter");

app.use("/administrators", adminRouter);
app.use("/candidates", candidateRouter);
app.use("/users", userRouter);
app.use("/companys", companyRouter);
app.use("/offers", jobRouter);
app.use("/candidacys", candidacyRouter);
app.use("/resumes", resumeRouter);
app.use("/Recommendations", recommendationRouter);
app.use("/categorys", categoryRouter);
app.use("/questions", questionRouter);

const port = process.env.PORT;
app.listen(port, () => {
  console.log(`Server started on port, ${port}`);
});
