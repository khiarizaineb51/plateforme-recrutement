const questionController = require ('../Controllers/QuestionController')

const Route = require ('express').Router()

Route.post("/", questionController.create);

Route.get("/",questionController.readAll);

Route.get("/:id",questionController.readOne);

Route.put("/:id",questionController.update);

Route.delete("/:id",questionController.delete);

module.exports=Route;