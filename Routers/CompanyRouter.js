const companyController= require ('../Controllers/CompanyController')

const Route=require('express').Router()

Route.post("/", companyController.create);

Route.get("/",companyController.readAll);

Route.get("/:id",companyController.readOne);

Route.put("/:id",companyController.update);

Route.delete("/:id",companyController.delete);

module.exports=Route;