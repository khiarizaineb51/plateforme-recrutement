const candidateController=require("../Controllers/CandidateController")

const Route=require('express').Router()


Route.post("/", candidateController.create);

Route.get("/",candidateController.readAll);

Route.get("/:id",candidateController.readOne);

Route.put("/:id",candidateController.update);

Route.delete("/:id",candidateController.delete);

module.exports=Route;