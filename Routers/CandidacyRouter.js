const candidacyController = require ('../Controllers/CandidacyController')

const Route = require ('express').Router()


Route.post("/", candidacyController.create);

Route.get("/",candidacyController.readAll);

Route.get("/:id",candidacyController.readOne);

Route.put("/:id",candidacyController.update);

Route.delete("/:id",candidacyController.delete);

module.exports=Route;