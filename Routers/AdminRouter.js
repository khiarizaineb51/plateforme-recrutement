const adminController= require('../Controllers/AdminController')

const Route=require('express').Router()

const upload = require('../Docs/Middelwares/uploadFile')

Route.post("/", adminController.create);

Route.get("/",adminController.readAll);

Route.get("/:id",adminController.readOne);

Route.put("/:id",adminController.update);

Route.delete("/:id",adminController.delete);

module.exports=Route;