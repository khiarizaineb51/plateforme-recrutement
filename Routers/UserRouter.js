const Usercontroller = require ('../Controllers/UserController')
 
const checkAuth = require ('../Docs/Middelwares/checkAuth')

const UserValidator= require ('../Validator/User')


const route = require('express').Router() 

route.post("/register",Usercontroller.signup)

route.post("/login",Usercontroller.login)

route.post("/refreshtoken",checkAuth, Usercontroller.refreshToken)

route.post("/logout",checkAuth, Usercontroller.logout)

module.exports=route