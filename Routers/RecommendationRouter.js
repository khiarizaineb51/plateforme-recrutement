const recommendationController = require ('../Controllers/RecommendationController')

const Route = require ('express').Router()

Route.post("/",recommendationController.create)
Route.get("/",recommendationController.create)
Route.get ("/:id", recommendationController.readOne)
Route.put("/:id",recommendationController.update)
Route.delete("/:id", recommendationController.delete)

module.exports=Route