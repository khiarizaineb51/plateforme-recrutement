const categoryController= require ('../Controllers/CategoryController')

const Route = require ('express').Router()


Route.post("/", categoryController.create);

Route.get("/",categoryController.readAll);

Route.get("/:id",categoryController.readOne);

Route.put("/:id",categoryController.update);

Route.delete("/:id",categoryController.delete);

module.exports=Route;