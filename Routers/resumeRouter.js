const Resumecontroller = require ('../Controllers/resumeController')

const Route= require ('express').Router()

Route.post("/", Resumecontroller.create)
Route.get("/",Resumecontroller.readAll)
Route.get("/:id",Resumecontroller.readOne)
Route.put("/:id",Resumecontroller.update)
Route.delete("/:id",Resumecontroller.delete)

module.exports=Route