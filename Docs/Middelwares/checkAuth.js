const jwt = require("jsonwebtoken");

function chek_auth(req, res, next) {
    
     console.log("headers : ",req.headers)
    try {
        const token = req.headers["authorization"]
        if (!token) {
            return res.status(403).json({ status: 403, message: 'No Token  ', data: null })
        }
        jwt.verify(token, "jobgate", function (err, decoder) {
            if (err) {
                return res.status(401).json({ status: 401, message: 'Auth failed ' + err, data: null })
            }
         req.visitor_data=decoder;
            next();
        })
    }
    catch (error) {
        return res.status(401).json({ status: 401, message: 'Auth failed ' + error, data: null })
    }
};
// For User Profile
const isUser = (req, res, next) => {
    chek_auth(req, res, () => {
      if (req.user._id === req.params.id || req.user.isAdmin || req.user.isCandidate || req.user.isCompany) {
        next();
      } else {
        res.status(403).send("Access denied. Not authorized...");
      }
    });
  };
  // For Admin
const isAdmin = (req, res, next) => {
    chek_auth(req, res, () => {
      if (req.user.isAdmin) {
        next();
      } else {
        res.status(403).send("Access denied. Not authorized...");
      }
    });
  };
  
  // For Candidate
const isCandidate = (req, res, next) => {
    chek_auth(req, res, () => {
      if (req.user.isCandidate) {
        next();
      } else {
        res.status(403).send("Access denied. Not authorized...");
      }
    });
  };

  // For Company
const isCompany = (req, res, next) => {
    chek_auth(req, res, () => {
      if (req.user.isCompany) {
        next();
      } else {
        res.status(403).send("Access denied. Not authorized...");
      }
    });
  };

module.exports = {chek_auth, isUser, isAdmin, isCandidate, isCompany}

